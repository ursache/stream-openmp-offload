# stream-openmp-offload

OpenMP offload version of the stream benchmark by Gilles Fourestey (EPFL), 
based on the work of John D. McCalpin (https://www.cs.virginia.edu/stream/).

## Latest results:

### nvc 21.2-0, Intel Xeon Gold 6132, V100 (32 GB)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          735137.5     0.001757     0.001741     0.001782
Scale:         734433.5     0.001757     0.001743     0.001792
Add:           790446.0     0.002459     0.002429     0.002481
Triad:         787585.7     0.002462     0.002438     0.002478
```
### gcc 11.2.0, Intel Xeon Gold 6131, V100 (32 GB)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          243115.0     0.005540     0.005265     0.008647
Scale:         258533.6     0.005255     0.004951     0.005490
Add:           331784.1     0.006087     0.005787     0.006325
Triad:         313409.8     0.006313     0.006126     0.006658
```
### xl 16.01 compilers, IBM Power9, V100 (16 GB)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          801060.7     0.001607     0.001598     0.001611
Scale:         799986.5     0.001607     0.001600     0.001614
Add:           839823.1     0.002294     0.002286     0.002298
Triad:         840963.2     0.002293     0.002283     0.002299
```
### cce 12.0.3 compiler, Intel Xeon 2690 P100 (16 GB)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          540546.6     0.002378     0.002368     0.002384
Scale:         540546.6     0.002379     0.002368     0.002396
Add:           556035.6     0.003465     0.003453     0.003467
Triad:         555882.1     0.003464     0.003454     0.003467
```
### cce 13.0.1 compiler, HPE, AMD MI100 (32 GB)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:         1034832.1     0.001251     0.001237     0.001295
Scale:        1033040.0     0.001253     0.001239     0.001299
Add:          1005250.7     0.001929     0.001910     0.001973
Triad:        1003247.0     0.001933     0.001914     0.001976
```

### gcc 9.3, Intel Xeon Gold 6230, A100 (40GB)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:          432298.0     0.004612     0.002961     0.017861
Scale:         432158.8     0.007530     0.002962     0.017868
Add:           478807.5     0.006801     0.004010     0.019977
Triad:         478949.9     0.006776     0.004009     0.015767
```

### nvcc 21.2-0, Intel Xeon Gold 6132, V100 (32 GB) CUDA
```
Function      Rate (MB/s)   Avg time     Min time     Max time
Copy:      808662.3166       0.0016       0.0016       0.0016
Scale:     808175.3906       0.0016       0.0016       0.0016
Add:       824095.7511       0.0023       0.0023       0.0023
Triad:     825108.9836       0.0023       0.0023       0.0023
```

### nvcc 21.2-0, A100 (40GB) CUDA
```
Function      Rate (MB/s)   Avg time     Min time     Max time
Copy:      1379421.6650       0.0009       0.0009       0.0009
Scale:     1376592.0821       0.0009       0.0009       0.0009
Add:       1399559.2075       0.0014       0.0014       0.0014
Triad:     1401507.7758       0.0014       0.0014       0.0014
```

### nvcc 23.1-0, Intel Xeon 8358 CPU, A100 (64GB) OpenACC (Leonardo)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:         1428608.1     0.000899     0.000896     0.000901
Scale:        1427089.1     0.000900     0.000897     0.000902
Add:          1442684.3     0.001339     0.001331     0.001347
Triad:        1445792.4     0.001338     0.001328     0.001347
```

### nvcc 23.1-0, Intel Xeon 8358 CPU, A100 (64GB) CUDA (Leonardo)
```
Function      Rate (MB/s)   Avg time     Min time     Max time
Copy:      1439718.1872       0.0009       0.0009       0.0009
Scale:     1434716.4939       0.0009       0.0009       0.0009
Add:       1447091.4070       0.0013       0.0013       0.0014
Triad:     1450218.5629       0.0013       0.0013       0.0013
```

### gcc 11.3-0, Intel Xeon 8358 CPU, A100 (64GB) OpenACC (Leonardo)
```
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:         1133835.1     0.001142     0.001129     0.001352
Scale:        1123866.3     0.001157     0.001139     0.002086
Add:          1225173.2     0.001583     0.001567     0.001969
Triad:        1221271.4     0.001592     0.001572     0.002050
```
